# Submission Queue

Before opening a ticket for submitting an app, please take your time to check if the app:

1. meets our
   [inclusion criteria](https://f-droid.org/docs/Inclusion_Policy/)

2. is already in the
   [fdroiddata repository](https://gitlab.com/search?scope=issues&project_id=36528)

3. has already [been requested](https://gitlab.com/search?scope=issues&project_id=2167965)

4. Consider packaging it yourself, then opening a merge request with
   the required metadata which will save you and us a lot of time, see
   the
   [contribution guideline](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md)
   and
   [fdroiddata readme](https://gitlab.com/fdroid/fdroiddata/blob/master/README.md)

5. Then, if you still have not found the app you are interested in,
   open a new [Request For Packaging](https://gitlab.com/fdroid/rfp/issues/new).

6. Consider [donating](https://f-droid.org/donate) to support F-Droid and the ongoing maintenance of the app:
<a href="https://liberapay.com/F-Droid-Data/donate" target="_blank"><img alt="Liberapay" src="https://f-droid.org/assets/liberapay_donate_button.svg" height="40"/></a>
<a href="https://opencollective.com/f-droid" target="_blank"><img alt="OpenCollective" src="https://f-droid.org/assets/opencollective_button.png" height="40"/></a>
[__More Options__](https://f-droid.org/donate)



## _issuebot_

The F-Droid [_issuebot_](https://gitlab.com/fdroid/issuebot) runs whenever an
issue is created or changed.  It scans all new submissions and posts its reports
directly to the issue.  Once _issuebot_ completes posting its report, it will
add the `fdroid-bot` label to the issue.  That tells _issuebot_ to ignore that
issue in future runs.  If you want _issuebot_ to run on an issue again, remove
the `fdroid-bot` label again (which you can do yourself if you have at least
"reporter" status for this repo, or you can ask someone to do it for you).  On
the next run, _issuebot_ will consider that issue as if it had not seen it.
